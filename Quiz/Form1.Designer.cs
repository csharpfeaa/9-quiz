﻿namespace Quiz
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.pnIntro = new System.Windows.Forms.Panel();
            this.btnStart = new System.Windows.Forms.Button();
            this.pnQuiz = new System.Windows.Forms.Panel();
            this.btnFinish = new System.Windows.Forms.Button();
            this.btnInapoi = new System.Windows.Forms.Button();
            this.btnInainte = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbFals = new System.Windows.Forms.RadioButton();
            this.rbAdevarat = new System.Windows.Forms.RadioButton();
            this.lblIntrebare = new System.Windows.Forms.Label();
            this.pbIntrebari = new System.Windows.Forms.ProgressBar();
            this.pnIntro.SuspendLayout();
            this.pnQuiz.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(282, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Test";
            // 
            // pnIntro
            // 
            this.pnIntro.Controls.Add(this.btnStart);
            this.pnIntro.Controls.Add(this.label1);
            this.pnIntro.Location = new System.Drawing.Point(12, 92);
            this.pnIntro.Name = "pnIntro";
            this.pnIntro.Size = new System.Drawing.Size(671, 231);
            this.pnIntro.TabIndex = 1;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(209, 129);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(245, 69);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start quiz";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // pnQuiz
            // 
            this.pnQuiz.Controls.Add(this.pbIntrebari);
            this.pnQuiz.Controls.Add(this.btnFinish);
            this.pnQuiz.Controls.Add(this.btnInapoi);
            this.pnQuiz.Controls.Add(this.btnInainte);
            this.pnQuiz.Controls.Add(this.groupBox1);
            this.pnQuiz.Controls.Add(this.lblIntrebare);
            this.pnQuiz.Location = new System.Drawing.Point(12, 12);
            this.pnQuiz.Name = "pnQuiz";
            this.pnQuiz.Size = new System.Drawing.Size(671, 380);
            this.pnQuiz.TabIndex = 2;
            // 
            // btnFinish
            // 
            this.btnFinish.Location = new System.Drawing.Point(182, 331);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(290, 46);
            this.btnFinish.TabIndex = 4;
            this.btnFinish.Text = "Am terminat";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnInapoi
            // 
            this.btnInapoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnInapoi.Location = new System.Drawing.Point(64, 163);
            this.btnInapoi.Name = "btnInapoi";
            this.btnInapoi.Size = new System.Drawing.Size(94, 49);
            this.btnInapoi.TabIndex = 3;
            this.btnInapoi.Text = "<";
            this.btnInapoi.UseVisualStyleBackColor = true;
            this.btnInapoi.Click += new System.EventHandler(this.btnInapoi_Click);
            // 
            // btnInainte
            // 
            this.btnInainte.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnInainte.Location = new System.Drawing.Point(493, 163);
            this.btnInainte.Name = "btnInainte";
            this.btnInainte.Size = new System.Drawing.Size(94, 49);
            this.btnInainte.TabIndex = 2;
            this.btnInainte.Text = ">";
            this.btnInainte.UseVisualStyleBackColor = true;
            this.btnInainte.Click += new System.EventHandler(this.btnInainte_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbFals);
            this.groupBox1.Controls.Add(this.rbAdevarat);
            this.groupBox1.Location = new System.Drawing.Point(229, 138);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Raspuns";
            // 
            // rbFals
            // 
            this.rbFals.AutoSize = true;
            this.rbFals.Location = new System.Drawing.Point(17, 57);
            this.rbFals.Name = "rbFals";
            this.rbFals.Size = new System.Drawing.Size(44, 17);
            this.rbFals.TabIndex = 3;
            this.rbFals.TabStop = true;
            this.rbFals.Text = "Fals";
            this.rbFals.UseVisualStyleBackColor = true;
            // 
            // rbAdevarat
            // 
            this.rbAdevarat.AutoSize = true;
            this.rbAdevarat.Location = new System.Drawing.Point(17, 25);
            this.rbAdevarat.Name = "rbAdevarat";
            this.rbAdevarat.Size = new System.Drawing.Size(68, 17);
            this.rbAdevarat.TabIndex = 2;
            this.rbAdevarat.TabStop = true;
            this.rbAdevarat.Text = "Adevarat";
            this.rbAdevarat.UseVisualStyleBackColor = true;
            this.rbAdevarat.CheckedChanged += new System.EventHandler(this.salveazaRaspunsRadio);
            // 
            // lblIntrebare
            // 
            this.lblIntrebare.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblIntrebare.Location = new System.Drawing.Point(64, 31);
            this.lblIntrebare.Name = "lblIntrebare";
            this.lblIntrebare.Size = new System.Drawing.Size(523, 24);
            this.lblIntrebare.TabIndex = 0;
            this.lblIntrebare.Text = "Intrebare";
            this.lblIntrebare.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbIntrebari
            // 
            this.pbIntrebari.Location = new System.Drawing.Point(229, 255);
            this.pbIntrebari.Name = "pbIntrebari";
            this.pbIntrebari.Size = new System.Drawing.Size(200, 23);
            this.pbIntrebari.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 404);
            this.Controls.Add(this.pnQuiz);
            this.Controls.Add(this.pnIntro);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnIntro.ResumeLayout(false);
            this.pnIntro.PerformLayout();
            this.pnQuiz.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnIntro;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel pnQuiz;
        private System.Windows.Forms.Label lblIntrebare;
        private System.Windows.Forms.Button btnInapoi;
        private System.Windows.Forms.Button btnInainte;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbFals;
        private System.Windows.Forms.RadioButton rbAdevarat;
        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.ProgressBar pbIntrebari;
    }
}

