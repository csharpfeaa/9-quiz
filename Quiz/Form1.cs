﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz
{
    public partial class Form1 : Form
    {
        private List<Intrebare> dbIntrebari = new List<Intrebare>();
        private int intrebareCurenta = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            pnQuiz.Visible = true;
            pnIntro.Visible = false;

            MessageBox.Show(dbIntrebari.Count + "intreb");
            pbIntrebari.Maximum = dbIntrebari.Count;
            pbIntrebari.Step = 1;
            afiseazaIntrebare();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pnQuiz.Visible = false;
            pnIntro.Visible = true;

            dbIntrebari.Add(new Intrebare("Un an are 367 zile", 0));
            dbIntrebari.Add(new Intrebare("Curcubeul are 7 culori", 1));
            dbIntrebari.Add(new Intrebare("Marea unire a avut loc in 1918", 1));
        }

        private void afiseazaIntrebare()
        {
            // schimbam progress barul
            pbIntrebari.Value = intrebareCurenta + 1;

            // punem textul
            lblIntrebare.Text = dbIntrebari[intrebareCurenta].intrebare;

            if (dbIntrebari[intrebareCurenta].raspunsUtilizator != -1)
            {
                if (dbIntrebari[intrebareCurenta].raspunsUtilizator == 1)
                {
                    rbAdevarat.Checked = true;
                    rbFals.Checked = false;
                }
                else
                {
                    rbAdevarat.Checked = false;
                    rbFals.Checked = true;
                }
            }
            else
            {
                // golim raspunsurile
                rbAdevarat.Checked = false;
                rbFals.Checked = false;
            }
        }

        private void btnInainte_Click(object sender, EventArgs e)
        {
            salveazaRaspuns();

            // afiseaza intrebarea urmatoare
            intrebareCurenta = (intrebareCurenta + 1) % dbIntrebari.Count;
            afiseazaIntrebare();
        }

        private void btnInapoi_Click(object sender, EventArgs e)
        {
            salveazaRaspuns();

            // afiseaza intrebarea urmatoare
            intrebareCurenta = (intrebareCurenta + (dbIntrebari.Count - 1)) % dbIntrebari.Count;
            afiseazaIntrebare();
        }

        void salveazaRaspuns()
        {
            if (rbAdevarat.Checked)
                dbIntrebari[intrebareCurenta].raspunsUtilizator = 1;
            else if (rbFals.Checked)
                dbIntrebari[intrebareCurenta].raspunsUtilizator = 0;
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            // ascunde panouri
            pnQuiz.Visible = false;
            pnIntro.Visible = true;

            // calculeaza scor
            int scor = 0;
            foreach (Intrebare intrebare in dbIntrebari)
            {
                if (intrebare.raspunsUtilizator == intrebare.raspunsCorect)
                    scor++;
            }

            MessageBox.Show("Ati raspuns corect la " + scor + " intrebari din " + dbIntrebari.Count);
        }

        private void salveazaRaspunsRadio(object sender, EventArgs e)
        {
            if (rbAdevarat.Checked)
                dbIntrebari[intrebareCurenta].raspunsUtilizator = 1;
            else if (rbFals.Checked)
                dbIntrebari[intrebareCurenta].raspunsUtilizator = 0;
        }
    }
}
