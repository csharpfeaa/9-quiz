﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    class Intrebare
    {
        public string intrebare;
        public int raspunsCorect;
        public int raspunsUtilizator = -1;

        public Intrebare(string v1, int v2)
        {
            this.intrebare = v1;
            this.raspunsCorect = v2;
        }
    }
}
